""" Strategy Python """

from abc import ABCMeta, abstractmethod

LIMIT_COUNT_ELEMENTS = 100
BORDER = 10


# abstract base class
class FileStrategy(metaclass=ABCMeta):
	@abstractmethod
	# algorithm interface
	def open(self, filename):
		pass

	@abstractmethod
	# algorithm interface
	def show(self, data):
		pass

	@abstractmethod
	# algorithm interface
	def filter(self, data):
		pass

	@abstractmethod
	# algorithm interface
	def save(self, data):
		pass


# interface
class FileOpen:
	def __init__(self, file):
		self.file = file.open
		self.data = list()

	def open(self, filename):
		self.data = self.file(filename)
		return self.data

# interface
class FileShow:
	def __init__(self, file):
		self.file = file.show

	def show(self, data):
		print(self.file(data))

# interface
class FileFilter:
	def __init__(self, file):
		self.file = file.filter

	def filter(self, data):
		self.data = self.file(data)
		return self.data

# interface
class FileStorage:
	def __init__(self, file):
		self.file = file.save
		self.data = list()

	def save(self, data):
		self.data = data

	


# strategy
class TextFile(FileStrategy):
	# algorithm interface
	# открытие файла  
	def open(self, filename):
		if filename is None:
			print('Файл недоступен!')
			return None

		self.data = list()
		with open(filename, 'r') as txt:
			for symbol in txt:
				self.data.append(int(symbol.rstrip()))

				if len(self.data) > LIMIT_COUNT_ELEMENTS - 1:
					return self.data
		return self.data

	def show(self, data):
		return data

	def filter(self, data):
		return [int(symbol) for symbol in data if int(symbol) > BORDER]

	def save(self, data):
		self.data = data


# main 

FILENAME = 'text.txt'


file = FileOpen(TextFile())
file.open(FILENAME)

storage = FileStorage(TextFile())
storage.save(file.data)

fileshow = FileShow(TextFile())
fileshow.show(storage.data)

filtr = FileFilter(TextFile())
print(filtr.filter(storage.data))